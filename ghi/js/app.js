function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
        <div class="col-4">
          <div class="card shadow mb-5">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-text">${description}</p>
            </div>
            <h6 class="card-subtitle">${location}</h6>
            <div class="card-footer">
              ${startDate} - ${endDate}
              </div>
          </div>
        </div>
      `;
}
function displayAlert(message, type) {
  const html = createAlert(message, type);
  const containerTag = document.querySelector('.container');
  const htmlElement = document.createElement('div');
  htmlElement.innerHTML = html;
  containerTag.append(htmlElement)}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const formattedStart = startDate.toLocaleDateString();
            const formattedEnd = endDate.toLocaleDateString();
            const location = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, formattedStart, formattedEnd, location);
            const row = document.querySelector('.row');

            row.innerHTML += html;
            // console.log(html);
          }
        }
      }
  } catch (e) {
    displayAlert(e.message, 'danger')
      }
  });
